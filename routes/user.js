var express = require("express");
const UserController = require("../controllers/UserController");

var router = express.Router();

router.get("/loadusers", UserController.usersList);

module.exports = router;