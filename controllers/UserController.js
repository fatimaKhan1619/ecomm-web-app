
const { body,validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const Knex = require('knex');

const connectWithTcp = (config) => {
    // Establish a connection to the database
    return Knex({
      client: 'pg',
      connection: {
        user: 'postgres', // e.g. 'my-user'
        password:'abcd@1234', // e.g. 'my-user-password'
        database:'postgres', // e.g. 'my-database'
        host: '35.239.215.122', // e.g. '127.0.0.1'
      },
    });
  }
  
  const connect = () => {
    let config = {pool: {}};
    config.pool.max = 5;
    config.pool.min = 5;
    config.pool.acquireTimeoutMillis = 60000; // 60 seconds
    config.createTimeoutMillis = 30000; // 30 seconds
    config.idleTimeoutMillis = 600000; // 10 minutes
    config.createRetryIntervalMillis = 200; // 0.2 seconds
  
    let knex;
      knex = connectWithTcp(config);
    return knex;
  };
  const getUsers = async (knex) => {
    return await knex
      .select('*')
      .from('users')
      .orderBy('id', 'desc');
  };


/**
 * Book List.
 * 
 * @returns {Object}
 */
exports.usersList = [
	async function (req, res) {
		try {
            let knex = connect()
            const users = await getUsers(knex);
            return apiResponse.successResponseWithData(res, "Operation success", users);
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];
